const mongoose = require("mongoose");
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;
const bcrypt = require("bcryptjs");

const userSchema = new Schema({
  expoPushToken: String,
  name: String,
  email: { type: String, unique: true },
  emailVerified: Date,
  role: { type: String, default: 'Client' },
  image: String,
  hashedPassword: String,
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  removed: { type: Boolean, default: false },
  enabled: { type: Boolean, default: true },
  surname: String,
  isLoggedIn: { type: Boolean, default: false },
  address: String,
  phone: String,
  nik: String,
});

// generating a hash
userSchema.methods.generateHash = function (hashedPassword) {
    return bcrypt.hashSync(hashedPassword, bcrypt.genSaltSync(), null);
};
  
  // checking if password is valid
  userSchema.methods.validPassword = function (hashedPassword) {
    return bcrypt.compareSync(hashedPassword, this.hashedPassword);
};
  
module.exports = mongoose.model("User", userSchema);