const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { PrismaClient } = require("@prisma/client");

const mongoose = require("mongoose");

const Admin = mongoose.model("Admin");

require("dotenv").config({ path: ".variables.env" });
const prisma = new PrismaClient();

exports.register = async (req, res) => {
  console.log("TEST ", req.body);
  try {
    let { email, name, nik, password, expoPushToken, phone} = req.body;
    // console.log("Masuk Data Sign Up : ",req.body)
    if (!email || !password || !nik)
      return res.status(400).json({ msg: "Not all fields have been entered." });

    // console.log("TEST");
    const existingAdmin = await prisma.user.findUnique({ where: { email: email } });
    console.log("Existing Admin : ", existingAdmin);
    if (existingAdmin)
      return res
        .status(400)
        .json({ msg: "An account with this email already exists." });
    
    const salt = await bcrypt.genSalt();
    const hashedPassword = await bcrypt.hash(password, salt);

    const newAdmin = await prisma.user.create({
      data: {
        email,
        name,
        nik,
        phone,
        hashedPassword,
        expoPushToken,
        emailVerified: new Date(), // Set emailVerified to the current date and time
      },
    });

    console.log("Data save : ", newAdmin);
    res.status(200).send({
      success: true,
      admin: {
        id: newAdmin._id,
        email: newAdmin.email,
        name: newAdmin.name,
        phone: newAdmin.phone,
        nik: newAdmin.nik,
        expoPushToken: newAdmin.expoPushToken,
        password: newAdmin.hashedPassword
      },
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      result: null,
      message: err.message,
    });
  }
};

exports.login = async (req, res) => {
  try {
    const { email, password } = req.body;
    console.log("Test Masuk Sini!!!",req.body);
    if (!email || !password)
    return res.status(400).json({ msg: "Not all fields have been entered." });
    const user = await prisma.user.findUnique({ where: { email } });

    if (!user)
      return res.status(400).json({
        success: false,
        result: null,
        message: "No account with this email has been registered.",
      });

    const isMatch = await bcrypt.compare(password, user.hashedPassword);
    if (!isMatch)
      return res.status(400).json({
        success: false,
        result: null,
        message: "Invalid credentials.",
      });

    const token = jwt.sign(
      {
        exp: Math.floor(Date.now() / 1000) + 60 * 60 * 24,
        id: user.id,
      },
      process.env.JWT_SECRET
    );

    const result = await prisma.user.update({
      where: { id: user.id },
      data: { isLoggedIn: true },
    });

    res.json({
      success: true,
      result: {
        token,
        user: {
          id: result._id,
          name: result.name,
          phone: result.phone,
          nik: result.nik,
          isLoggedIn: result.isLoggedIn,
        },
      },
      message: "Successfully logged in as user",
    });
  } catch (err) {
    res.status(500).json({ success: false, result: null, message: err.message });
  }
};

exports.isValidToken = async (req, res, next) => {
  try {
    const token = req.header("x-auth-token");
    if (!token)
      return res.status(401).json({
        success: false,
        result: null,
        message: "No authentication token, authorization denied.",
        jwtExpired: true,
      });

    const verified = jwt.verify(token, process.env.JWT_SECRET);
    if (!verified)
      return res.status(401).json({
        success: false,
        result: null,
        message: "Token verification failed, authorization denied.",
        jwtExpired: true,
      });

    const admin = await Admin.findOne({ _id: verified.id });
    if (!admin)
      return res.status(401).json({
        success: false,
        result: null,
        message: "Admin doens't Exist, authorization denied.",
        jwtExpired: true,
      });

    if (admin.isLoggedIn === false)
      return res.status(401).json({
        success: false,
        result: null,
        message: "Admin is already logout try to login, authorization denied.",
        jwtExpired: true,
      });
    else {
      req.admin = admin;
      // console.log(req.admin);
      next();
    }
  } catch (err) {
    res.status(500).json({
      success: false,
      result: null,
      message: err.message,
      jwtExpired: true,
    });
  }
};

exports.logout = async (req, res) => {
  const result = await Admin.findOneAndUpdate(
    { _id: req.admin._id },
    { isLoggedIn: false },
    {
      new: true,
    }
  ).exec();

  res.status(200).json({ isLoggedIn: result.isLoggedIn });
};
